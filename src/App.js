import { useEffect, useState } from 'react';
// React router
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import { UserProvider } from './UserContext';

// // Import the Bootstrap CSS.
import 'bootstrap/dist/css/bootstrap.min.css';
// Style
import './App.css';

// Components
import AppNavbar from './components/AppNavbar';

// Pages
import About from "./pages/About";
import Cart from './pages/Cart';
import Contact from "./pages/Contact";
import Dashboard from "./pages/Dashboard";
import Error from './pages/Error';
import ForgotPassword from './pages/ForgotPassword';
import Home from './pages/Home';
import Login from './pages/Login';
import Logout from './pages/Logout';
import OrderLog from './pages/OrderLog';
import Products from './pages/Products';
import ProductView from './pages/ProductView';
import Register from './pages/Register';
import UpdateProduct from './pages/UpdateProduct';


function App() {
  // To store the user information and will be used for validating if a suser is already logged in on the app or not.
  const [user, setUser] = useState({
    // email: localStorage.getItem("email")
    id: null,
    isAdmin: null
  });

  // Get the cart in the localStorage and set it as initial value for cart
  const cartFromLocalStorage = JSON.parse(localStorage.getItem("cart")) || [];
  const [cart, setCart] = useState(cartFromLocalStorage);

  // function for clearing localStorage on logout
  const unSetUser = () => {
    localStorage.clear();
  }

  // add item to local Storage
  const addToCart = (order) => {
    let cartCopy = [...cart];
    let { productId } = order;
    let existingItem = cartCopy.find(cartOrder => cartOrder.productId == productId);
      //if item already exists
    if(existingItem){
      existingItem.quantity += order.quantity; //update item
    } 
    else{ //if item doesn't exist, simply add it
      cartCopy.push(order)
    }

    // update app state
    setCart(cartCopy)

    //make cart a string and store in local space
    let stringCart = JSON.stringify(cartCopy);
    localStorage.setItem("cart", stringCart)
  }

  // update a quantity in of order
  const updateItem = (productId, amount) => {

    let cartCopy = [...cart]
    //find if item exists, just in case
    let existingtItem = cartCopy.find(item => item.productId == productId);
    //if it doesnt exist simply return
    if (!existingtItem) return
    
    //continue and update quantity
    existingtItem.quantity += amount;

    //validate result
    if (existingtItem.quantity <= 0) {
      //remove item  by filtering it from cart array
      cartCopy = cartCopy.filter(item => item.productId != productId)
    }
    //again, update state and localState
    setCart(cartCopy);
    
    let cartString = JSON.stringify(cartCopy);
    localStorage.setItem('cart', cartString);

  }

  const changeQuantity = (productId, amount) => {
    let cartCopy = [...cart];
    let existingtItem = cartCopy.find(item => item.productId == productId);
    //if it doesnt exist simply return
    if (!existingtItem) return
    existingtItem.quantity = amount;

    if (existingtItem.quantity <= 0) {
      //remove item  by filtering it from cart array
      cartCopy = cartCopy.filter(item => item.productId != productId)
    }

    //again, update state and localState
    setCart(cartCopy);
    let cartString = JSON.stringify(cartCopy);
    localStorage.setItem('cart', cartString);
  }

  // remove an order in cart
  const removeItem = (productId) => {
    //create cartCopy
    let cartCopy = [...cart]
    
    cartCopy = cartCopy.filter(item => item.productId != productId);
    
    //update state and local
    setCart(cartCopy);
    
    let cartString = JSON.stringify(cartCopy)
    localStorage.setItem('cart', cartString)
  }

  useEffect(()=> {
    localStorage.setItem("cart", JSON.stringify(cart))
  }, [cart])

  // To update the User State upon age load if a user already exist.
  useEffect( () => {
      fetch(`${process.env.REACT_APP_API_URL}/users/profile`, {
          headers: {
              "Authorization": `Bearer ${localStorage.getItem("token")}`
          }
      })
      .then(res => res.json())
      .then(data => {
          // console.log(data);

          // set the user states values with user details upon successful login
          if(typeof data._id !== "undefined"){
              setUser({
                  id: data._id,
                  isAdmin: data.isAdmin
              })
          }
          else{
              setUser({
                  id: null,
                  isAdmin: null
              })
          }
      })
  }, []) 
  


  return (
    <UserProvider value={{user, setUser, unSetUser, cart, setCart, addToCart, updateItem, removeItem, setCart, changeQuantity}}>
      <Router>
        <AppNavbar />
          <Routes>
            <Route exact path="/" element={<Home />} />
            <Route exact path='/login' element={<Login />} />
            <Route exact path="/logout" element={<Logout />} />
            <Route exact path='/register' element={<Register />} />
            <Route exact path="/passwordReset" element={<ForgotPassword />} />

            <Route exact path="/dashboard" element={<Dashboard />} />

            <Route exact path="/products" element={<Products />} />
            <Route exact path='/products/:productId' element={<ProductView />}/>
            <Route exact path='/products/:productId/update' element={<UpdateProduct />} />

            <Route exact path='/about' element={<About />} />
            <Route exact path='/contact' element={<Contact />} />

            <Route exact path="/cart" element={<Cart />} />
            <Route exact path='/orders/history' element={<OrderLog /> } />
            <Route exact path="*" element={<Error />} />
          </Routes>
      </Router>
      <div style={{backgroundColor: "hsl(0deg, 0%, 10%)", textAlign: "center", color: "white"}}>
        <p style={{margin: "0px"}}>© 2022 PCByte All Rights Reserved </p>
      </div>
    </UserProvider>
  );
}

export default App;


import style from "../styles/about.module.css";

export default function About() {
  return (
    <div className={style.container}>
      
      <div className={style.aboutContainer}>
        <h1 style={{fontSize: "2rem"}}>About</h1>
        <p style={{marginBottom: "1rem"}}>PCByte is an E-commerce website wherein you can find the best deals for your dream computer. 
          PCByte also have a physical store, you can pick-up your order by yourself.</p>

        <p style={{marginBottom: "1rem"}}>Welcome to my world. You will be greeted by the unexpected here and your mind will be challenged 
          and expanded in ways that you never thought possible. That is if you are able to survive...</p>

        <p>You can Visit Our Shop at Lot 2 D-2 Unit C National Rd. Alabang, Muntinlupa City</p>

      </div>

    </div>
  )
}

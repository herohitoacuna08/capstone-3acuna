import React, { useEffect, useState } from 'react'
import { Button } from 'react-bootstrap';
import { useNavigate, useParams } from 'react-router-dom';
import Swal from 'sweetalert2';

import Img from "../assets/amd_pc.png";

export default function UpdateProduct() {

  const {productId} = useParams();
  const navigate = useNavigate();

  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [price, setPrice] = useState(1);
  const [stocks, setStocks] = useState(1);

  const [active, setActive] = useState(false);

  const fetchSpecificProduct = async (productId) => {

    await fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
    .then(res => res.json())
    .then(data => {
        setName(data.name);
        setDescription(data.description);
        setPrice(data.price);
        setStocks(data.stocks);
    }) 
  }

useEffect(() => {
    fetchSpecificProduct(productId);
}, [])


  const updateProduct = () => {

    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/update`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        "Authorization": `Bearer ${localStorage.getItem("token")}`
      },
      body: JSON.stringify({
        name: name,
        description: description,
        price: price,
        stocks: stocks
      })
    })
    .then(res => res.json())
    .then(data => {
      if(data){
        Swal.fire({
            title: "Update Successful!",
            icon: "success",
            text: "Product has been updated!"
        })
        navigate("/dashboard")
    }
    else{
        Swal.fire({
            title: "Update Failed",
            icon: "error",
            text: "Something went wrong. Please try again"
        })
    }
    })

  }

  useEffect(() => {
    if(name !== "" && description !== "" && price !== "" && stocks !== ""){
      setActive(true);
    }
    else setActive(false);
  }, [name, description, price, stocks])


  return (
    <>
    <div style={{backgroundColor: "hsl(0deg, 0%, 14%)", minHeight: "100vh"}}>
        <h1>Update Product</h1>
        <div style={{display: "flex", justifyContent: "center", padding: "20vh 10rem"}}>
            <img src={Img} alt="" style={{width: "25rem", height: "20rem", borderRadius: "10px"}}/>

            <div style={{display: "flex", justifyContent: "center", flexDirection: "column",marginLeft: "2rem", color: "white"}}>

                <label htmlFor="name">Product Name :</label>
                <input type="text" placeholder="Product Name" value={name} onChange={(e) => setName(e.target.value)}/>
                <label htmlFor="description">Description :</label>
                <textarea name="description" id="description" value={description} onChange={(e) => setDescription(e.target.value)}  cols="70" rows="8" placeholder="Description"></textarea>
                <label htmlFor="name">Stocks Left :</label>
                <input type="number" value={stocks} onChange={(e) => setStocks(e.target.value)}  placeholder="Stocks"/>
                <label htmlFor="name">Price (₱) :</label>
                <input type="number" value={price} onChange={(e) => setPrice(e.target.value)}  placeholder="Price"/>
                {
                active ? 
                <Button style={{width: "100%"}} onClick={() => updateProduct()}>Update</Button>
                :
                <Button style={{width: "100%"}} disabled>Update</Button>
            }
            </div>
        </div>
    </div>
    </>
  )
}

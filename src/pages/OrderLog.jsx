import React from 'react'
import { Table } from 'react-bootstrap';

// const checkOutContainer = {
//   position: "fixed", 
//   bottom: "0", 
//   display: "flex",
//   height: "12vh",
//   justifyContent: "center",
//   alignItems: "center",
//   backgroundColor: "hsl(0deg, 0%, 18%)", 
//   width: "100%",
//   color: "white"
// }

export default function OrderLog({orderLog}) {

  

  const Purchase = orderLog.map((order, idx) => {
    const { productName, price, quantity, totalAmount, orderDate} = order;
    return(
      <tr key={idx}>
        <td>{orderDate}</td>
        <td>{productName}</td>
        <td>₱{price.toLocaleString()}</td>
        <td>{quantity}</td>
        <td>₱{totalAmount.toLocaleString()}</td>
      </tr> 
    )
  })

  return (
    <div>
      <Table striped bordered hover variant='dark' className='my-4'>
            <thead>
                <tr>
                    <th>Date </th>
                    <th>Name</th>
                    <th>Price</th>
                    <th>Quantityl</th>
                    <th>Total</th>
                </tr>
            </thead>
            <tbody>
              {Purchase}
            </tbody>
        </Table>
    </div>
  )
}

import { Navigate } from "react-router-dom";
import { useContext, useEffect } from "react";
import UserContext from "../UserContext";

export default function Logout(){

    const {user, setUser, unSetUser, setCart} = useContext(UserContext);

    unSetUser();

    useEffect( () => {
        setCart([]);
        setUser({
            id: null,
            isAdmin: null
        })
    })

    return(
            <Navigate to="/login"/>
    )

}
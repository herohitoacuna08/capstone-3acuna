import { Button } from "react-bootstrap";
import { useState,useEffect, useContext } from "react";
import { useNavigate, useParams } from "react-router-dom";
import UserProvider from "../UserContext";

import style from "../styles/productView.module.css";

import img from "../assets/amd_pc.png";
import Swal from "sweetalert2";
import userEvent from "@testing-library/user-event";

const container = {
    height: "1.4rem",
    display: "inline-block",
    marginLeft: "0.5rem"
}

const inputStyle = {
    textAlign: "center",
    width: "3.2rem",
    height: "100%",
    margin: "0px"
}

const spanStyle = {
    padding: "0 0.2rem", 
    fontSize: "1.2rem", 
    fontWeight: "500", 
    cursor: "pointer", 
}


export default function ProductView() {

    const {user, addToCart} = useContext(UserProvider)
    const {productId} = useParams();
    const navigate = useNavigate();

    const [name, setName] = useState("");
    const [stocks, setStocks] = useState(0);
    const [price, setPrice] = useState(0);
    const [description, setDescription] = useState("");
    const [quantity, setQuantity] = useState(1)

    const order = {
        productId: productId,
        productName: name,
        price: price,
        quantity: quantity
    }

    const fetchSpecificProduct = (productId) => {
        fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
        .then(res => res.json())
        .then(data => {
            setName(data.name);
            setStocks(data.stocks);
            setPrice(data.price);
            setDescription(data.description);
        }) 
    }

    const buyNow = () => {
        fetch(`${process.env.REACT_APP_API_URL}/orders/`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${localStorage.getItem("token")}`
            },
            body: JSON.stringify({
                productId: productId,
                quantity: 1
            })
        })
        .then(res => res.json())
        .then(data => {
            if(data){
                Swal.fire({
                  title: "Thank You For Purchase",
                  icon: "success",
                  text: "Order has been created!"
                })
          
                // redirect to login page
                navigate("/products")
              }
              else{
                Swal.fire({
                  title: "Something went wrong",
                  icon: "error",
                  text: "Please try again."
                })
              }
        })
    }

    useEffect(() => {
        fetchSpecificProduct(productId);
    }, [])
    return (
        <>
            <div className={style.mainContainer}>
                <div className={style.container}>

                    <div>                        
                        <img src={img} className={style.img} alt="img"/>
                    </div>
                    <div className={style.containerBody}>
                        <h1 className={style.title}>{name}</h1>
                        <h6>Stocks: {stocks}</h6>
                        {
                            (user.isAdmin !== true && user.isAdmin !== null) && 
                            <h6>Quantity: 
                                <div style={container}>
                
                                    <span style={spanStyle} onClick={() => setQuantity(quantity - 1)}>-</span>
                                    <input type="number" style={inputStyle} value={order.quantity} onChange={(e) => setQuantity(parseInt(e.target.value))}/>
                                    <span style={spanStyle} onClick={() => setQuantity(quantity + 1)}>+</span>

                                </div>
                            </h6>
                        }
                        <h3 className={style.price}>₱{price.toLocaleString()}</h3>
                        {
                            (user.isAdmin !== true && user.isAdmin !== null) &&

                            <div className="positionBtn">
                                <Button className="me-2" onClick={() => addToCart(order)}>Add to cart</Button> 
                                <Button variant="success" onClick={() => buyNow(productId)}>Buy Now</Button>
                            </div>
                        }
                        
                    </div>
                </div>
                <div className={style.description}>
                        <h3>Description</h3>
                        <br></br>
                        <p>Product Name: {name}</p>
                        <p style={{margin: "0%"}}>{description}</p>
                </div>
            </div>
            
            
        
        </>
    )

    



}

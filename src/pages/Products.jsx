import { useState, useEffect } from "react"

import Card from "../components/Card";



export default function Products() {

    const [ products, setProducts ] = useState([]);

    const ProductsCard = products.map(product => {
        return <Card key={product._id} productData={product} />
    })

    const fetchProducts = async () => {

        await fetch(`${process.env.REACT_APP_API_URL}/products/`)
        .then(res => res.json())
        .then(data => setProducts(data))
    }

    useEffect(() => {

        fetchProducts();

    }, [])

    return (
        <>
            <h1 style={{ color: "white"}}>Products</h1>
            
            <div style={{display: "flex", padding: "20px 10rem", justifyContent: "center",flexWrap: "wrap", backgroundColor: "hsl(0deg 0% 14%)", minHeight: "100vh"}}>
                {ProductsCard}
            </div>

        </>
    )



}

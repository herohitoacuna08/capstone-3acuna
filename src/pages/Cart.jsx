import { useContext, useEffect, useState } from "react";
import Table from "react-bootstrap/esm/Table";
import { Button } from "react-bootstrap";
import UserProvider from "../UserContext";
import OrderLog from "./OrderLog";

import style from "../styles/about.module.css";
import Swal from "sweetalert2";
import { useNavigate } from "react-router-dom";

const container = {
  padding:"15vh 10rem", 
  minHeight: "100vh", 
  width: "100%", 
  backgroundColor: "hsl(0deg, 0%, 16%)",
  color: "white"
}

const inputStyle = {
  textAlign: "center",
  width: "3.2rem",
  height: "1.6rem",
  margin: "0px"
}

const spanStyle = {
  padding: "0 0.2rem", 
  fontSize: "1.2rem", 
  fontWeight: "500", 
  cursor: "pointer", 
}

const checkOutContainer = {
  position: "fixed", 
  bottom: "0", 
  display: "flex",
  height: "12vh",
  justifyContent: "center",
  alignItems: "center",
  backgroundColor: "hsl(0deg, 0%, 18%)", 
  width: "100%",
  color: "white"
}

export default function Cart() {

  const navigate = useNavigate();
  const {cart, setCart, updateItem, removeItem, changeQuantity} = useContext(UserProvider);
  const [orderLog, setOrderLog] = useState([]);
  const [page, setPage] = useState("shoppingCart");
  const [isCheckoutActive, setIsCheckoutActive] = useState(false);

  // shopping cart orders
  const Orders = cart.map((order) => {
    const {productId, productName, price, quantity} = order;
    return(
      <tr key={productId}>
        <td>{productName}</td>
        <td>₱{price.toLocaleString()}</td>
        <td>

          <span style={spanStyle} onClick={() => updateItem(productId, -1)}>-</span>
          <input style={inputStyle} type="number" value={quantity} onChange={(e) => changeQuantity(productId, parseInt(e.target.value))}/>
          <span style={spanStyle} onClick={() => updateItem(productId, 1)}>+</span>

        </td>
        <td>₱{(price * quantity).toLocaleString()}</td>
        <td>
          <Button variant="danger" size="xs" onClick={() => removeItem(productId)}>Remove</Button>
        </td>
      </tr>
      
    )
  })
  
  // Total Sum
  const subTotal = cart.map(order => order.price * order.quantity);
  const totalSum = () => {
   if(subTotal.length !== 0) {
    const total = subTotal.reduce((acc, cur) => acc + cur);
    return total
   }
   return 0;
  }

   // Total puchase history 
  const pruchaseSubTotal = orderLog.map(order => order.totalAmount);
  const purchaseTotal = () => {
    if(pruchaseSubTotal.length !== 0) {
      const total = pruchaseSubTotal.reduce((acc, cur) => acc + cur);
      return total
    }
    return 0;
  }
 
  const PurchaseHistory = () => {
     fetch(`${process.env.REACT_APP_API_URL}/users/profile`, {
       headers: {
         "Authorization": `Bearer ${localStorage.getItem("token")}`
       }
     })
     .then(res => res.json())
     .then(data => {
       setOrderLog(data.purchaseLog);
     })
  }

  const checkOutButton = () => {  

    console.log(cart)

    fetch(`${process.env.REACT_APP_API_URL}/orders/checkout`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        "Authorization": `Bearer ${localStorage.getItem("token")}`
      },
      body: JSON.stringify(cart)
    })
    .then(res => res.json())
    .then(data => {
      // console.log(data)

      if(data){
        Swal.fire({
          title: "Order Created",
          icon: "success",
          text: "Thank you for Puchasing!"
        });

        // redirect to login page
        navigate("/products");

        // set cart to empty array
        setCart([]);

      }
      else{
        Swal.fire({
          title: "Something went wrong",
          icon: "error",
          text: "Please try again."
        })
      }

    })

  }


 
  useEffect(() => {
     PurchaseHistory();

     if(cart.length > 0){
      setIsCheckoutActive(true)
     }
     else{
      setIsCheckoutActive(false);
     }
  }, [])

  
  return (
    <>
    <div style={container}>
      <div style={{display: "flex", alignItems: "center", justifyContent:"space-between"}}>
        <h1 style={{cursor: "pointer"}} onClick={() => setPage("shoppingCart")}>Shopping Cart</h1>
        <h4 style={{padding: "0.5rem 2rem", border: "1px solid grey", cursor: "pointer"}} onClick={() => setPage("purchaseHistory")}>Purchase History</h4>
      </div>
      {
        page === "purchaseHistory" && <OrderLog orderLog={orderLog} />
      }
      
      {
        page === "shoppingCart" &&
        
        <Table striped bordered hover variant='dark' className='my-4'>
        <thead>
            <tr>
                <th>Product Name</th>
                <th>Price</th>
                <th>Quantity</th>
                <th>Sub Total</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
          {Orders}
        </tbody>
      </Table>
        // {
        //   (cart.length >= 2) && <Button onClick={() => setCart([])}>Remove all</Button>
        // }
      }
    </div>
    
    {
       page === "purchaseHistory" && 
       <div style={checkOutContainer}>
        <h3>Total Purchase: <span style={{fontSize: "2.3rem"}}>₱ {purchaseTotal().toLocaleString()}</span></h3>
      </div>

    }
    {
      page === "shoppingCart" &&

      <div style={checkOutContainer}>
        {
          isCheckoutActive ? 
          <Button style={{
            width: "40rem",
            marginRight: "2rem",
            height: "3.25rem",
            fontSize: "2.12"
          }} onClick={() => checkOutButton()}>Checkout</Button>
          :
          <Button style={{
            width: "40rem",
            marginRight: "2rem",
            height: "3.25rem",
            fontSize: "2.12"
          }} onClick={() => checkOutButton()} disabled>Checkout</Button>
        }
        <h3>Total: <span style={{fontSize: "2.3rem"}}>₱ {totalSum().toLocaleString()}</span></h3>
      </div>
    }

    </>
  )
}


import style from "../styles/contact.module.css"

export default function Contact() {
  return (
    <div className={style.container}>
       <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1932.246913672472!2d121.04495350080987!3d14.398672605497879!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3397d059ef238777%3A0x3ac553656bcb6feb!2s2%20d%2C%202a%20National%20Road%2C%20Putatan%2C%20San%20Pedro%2C%20Laguna!5e0!3m2!1sen!2sph!4v1659486226678!5m2!1sen!2sph" 
       width="400" 
       height="450" 
      className={style.mapContainer}
       allowFullScreen="" 
       loading="lazy" 
       referrerPolicy="no-referrer-when-downgrade"
       title="Google Map"></iframe>


      <div style={{color: "white", padding: "0 5rem",height: "450px"}}>

        <div>

          <h4>Location</h4>
          <p>Lot 2 D-2 Unit C National Rd. Alabang, Muntinlupa City</p>
        </div>

        <div>

          <h4>Mobile No.</h4>
          <p>09297633186 ( Smart )</p>
          <p>09357631846 ( Globe )</p>
        </div>

        <div>
          <h4>Message Us</h4>
        </div>


      </div>
    </div>

    

  )
}

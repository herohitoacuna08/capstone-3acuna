import { useEffect, useState } from "react";
import {Button} from 'react-bootstrap/esm';

import AllOrders from '../components/AllOrders';
import AllProducts from "../components/AllProducts";
import AllUsers from '../components/AllUser';
import CreateProduct from "../components/CreateProduct";


export default function Dashboard() {
 
  const [openPage, setOpenPage] = useState("orders");
  
  return (
    <>
    
    <div style={{padding: "15vh 5rem 2rem", color: "white", backgroundColor: "hsl(0deg, 0%, 14%)", minHeight: "100vh", width: "100%", textAlign: "center"}}>
        <h1>Dashboard</h1>
        <Button className='mx-2' style={{width: "7rem"}} onClick={() => setOpenPage("orders")}>Orders</Button>
        <Button className='mx-2' style={{width: "7rem"}} onClick={() => setOpenPage("products")}>Products</Button>
        <Button className='mx-2' style={{width: "7rem"}} onClick={() => setOpenPage("users")}>Users</Button>
        <Button variant='success' style={{position: "absolute", right: "5rem"}} onClick={() => setOpenPage("createProduct")}>Create Product</Button>

        {(openPage === "createProduct") && <CreateProduct />}
        {(openPage === "orders") && <AllOrders />}
        {(openPage === "products") && <AllProducts />}
        {(openPage === "users") && <AllUsers />}
        
    </div>

    
    </>
  )
}

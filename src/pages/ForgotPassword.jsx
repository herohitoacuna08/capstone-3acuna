import { useEffect, useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import Swal from "sweetalert2";
import style from "../styles/login.module.css";

export default function Login() {

  const navigate = useNavigate();

  const [email, setEmail] = useState("");
  const [newPass, setNewPass] = useState("");
  const [resetIsActive, setResetIsActive] = useState(false)

  useEffect(() => {

    if(email !== "" && newPass !== ""){
      setResetIsActive(true);
    }
    else setResetIsActive(false);

  }, [email, newPass])


  const resetPass = (e) => {

    e.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/users/password`, {
      method: "PATCH",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        email: email,
        password: newPass
      })
    })
    .then(res => res.json())
    .then(data => {

      console.log(data);

      if(data){
        Swal.fire({
          title: "Password Successfully Reset",
          icon: "success",
          text: ""
        })
        navigate("/login")
      }
      else{
        Swal.fire({
          title: "Email Not found",
          icon: "error",
          text: ""
        })
      }

    })

  }


  return (
    <div className={style.container}>

      <form className={style.loginForm} onSubmit={(e) => resetPass(e)}>

        <p className={style.loginLabel}>Reset Password</p>

        <label htmlFor="email">Email</label>
        <input className={style.email} type="email" id="email" placeholder="Enter your Email" value={email} onChange={(e) => setEmail(e.target.value)}/>
        <label htmlFor="password">New Password</label>
        <input className={style.password} type="password" id="password" placeholder="Enter your New Password" value={newPass} onChange={(e) => setNewPass(e.target.value)}/>
        {
          resetIsActive 
          ?
          <button className={style.btn} type="submit">Reset</button>
          :
          <button className={style.btn} type="submit" disabled>Reset</button>
        }
        
          <p>Don't have an account? Register  
            <Link to="/register" style={{marginLeft: "0.2rem", color: "blue"}}>
               here.
            </Link>
          </p>
        
      </form>

    </div>
  )
}


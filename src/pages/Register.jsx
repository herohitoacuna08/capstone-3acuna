
import { useEffect, useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import Swal from "sweetalert2";
import style from "../styles/register.module.css";

export default function Register(){

  const navigate = useNavigate();

  //State hooks to store the values of the input fields
	const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");
  const [username, setUsername] = useState("");
	const [mobileNo, setMobileNo] = useState("");
	const [email, setEmail] = useState("");
	const [password1, setPassword1] = useState("");
	const [password2, setPassword2] = useState("");
  const [registerIsActive, setRegisterIsActive] = useState(false);

  // render the register button
  useEffect(()=>{
		if((firstName !== "" && lastName !== "" && username !=="" && email !== "" && mobileNo.length === 11 && password1 !== "" && password2 !== "") && (password1 === password2)){
			setRegisterIsActive(true);
		}
		else{
			setRegisterIsActive(false);
		}
	},[firstName, lastName, username, email, mobileNo, password1, password2])

// Function to simulate user registration
function registerUser(e){
  //prevents the page redirection via form submit
  e.preventDefault();

  //Checking if the email is still available
  fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
    method: "POST",
    headers:{
      "Content-Type":"application/json"
    },
    body: JSON.stringify({
      firstName: firstName,
      lastName: lastName,
      username: username,
      mobileNo: mobileNo,
      email: email,
      password: password1
    })
  })
  .then(res => res.json())
  .then(data => {
    // console.log(data);
    
    if(data){
      Swal.fire({
        title: "Registration Successful",
        icon: "success",
        text: "Welcome to PCByte!"
      })

      // redirect to login page
      navigate("/login")
    }
    else{
      Swal.fire({
        title: "Something went wrong",
        icon: "error",
        text: "Please try again."
      })
    }
  })
}

  return (
    <div className={style.container}>

      <form className={style.registerForm} onSubmit = {(e) => registerUser(e)}>

        <p className={style.registerLabel}>Register</p>

        <div style={{display: "flex"}}>

          <div style={{display: "flex", flexDirection: "column", marginRight: "8px", width: "50%"}}>

            <label htmlFor="firstName">First Name</label>
            <input type="text" id="firstName" placeholder="First Name" value={firstName} onChange={(e) => setFirstName(e.target.value)} />

          </div>

          <div style={{display: "flex", flexDirection: "column", width: "50%"}}>
            <label htmlFor="lastName">Last Name</label>
            <input type="text" id="lastName" placeholder="Last Name" value={lastName} onChange={(e) => setLastName(e.target.value)}/>
          </div>

        </div>

        <label htmlFor="username">Username</label>
        <input type="text" id="username" placeholder="@username" value={username} onChange={(e) => setUsername(e.target.value)}/>

        <label htmlFor="mobileNo">Mobile No.</label>
        <input type="number" id="mobileNo" placeholder="09XX-XXX-XXX" value={mobileNo} onChange={(e) => setMobileNo(e.target.value)}/>

        <label htmlFor="email">Email</label>
        <input type="email" id="email" placeholder="example@mail.com"value={email} onChange={(e) => setEmail(e.target.value)}/>
        
        <label htmlFor="password1">Password</label>
        <input type="password" id="password1" placeholder="Enter Password" value={password1} onChange={(e) => setPassword1(e.target.value)}/>

        <label htmlFor="password2">Verify Password</label>
        <input type="password" id="password2" placeholder="Verify Password" value={password2} onChange={(e) => setPassword2(e.target.value)}/>

        {
          registerIsActive ?
          <button className={style.btn} type="submit">Create an account</button>
          :
          <button className={style.btn} type="submit" disabled>Create an account</button>
        }
        <p>Already have an account? Login 
          <Link to="/login" style={{marginLeft: "0.2rem", color: "blue"}}>
              here.
          </Link>
        </p>

      </form>

    </div>
  )
}

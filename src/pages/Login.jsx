
import { useContext, useEffect, useState } from "react";
import { Link, Navigate } from "react-router-dom";
import UserContext from '../UserContext';
import style from "../styles/login.module.css";
import Swal from "sweetalert2";

export default function Login() {
  
  // Allows us to consume the User Context object and it's properties to use for the user validation.
  const {user, setUser} = useContext(UserContext);


  // State hooks to store the values of input fields
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [loginIsActive, setLoginIsActive] = useState(false);

  // render the enable or disabled login button
  useEffect( () => {

    if(email !== "" && password !== ""){
      setLoginIsActive(true);
    }
    else{
      setLoginIsActive(false);
    }

  }, [email, password]);



  function loginUser(e){
    e.preventDefault();
    fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
        method: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify({
            // values are coming from our State hooks
            email: email,
            password: password
        })
    })
    .then(res => res.json())
    .then(data => {
        // console.log(data)
        // console.log(data.access);

        if(typeof data.access !== "undefined"){
            localStorage.setItem("token", data.access)
            retrieveUserDetails(data.access);

            Swal.fire({
                title: "Login Successful",
                icon: "success",
                text: "Welcome to PCByte!"
            })
        }
        else{
            Swal.fire({
                title: "Authentication Failed",
                icon: "error",
                text: "Check your login details and try again"
            })
        }

    })

    // Clear input fields;
    setEmail("");
    setPassword("");
  }

  // Retrieve User details
  // we will get the payload from the token.
  function retrieveUserDetails(token){
      fetch(`${process.env.REACT_APP_API_URL}/users/profile`, {
          headers: {
              "Authorization": `Bearer ${token}`
          }
      })
      .then(res => res.json())
      .then(data => {
          // console.log(data);

          setUser({
              id: data._id,
              isAdmin: data.isAdmin
          })
      })
  }

  // console.log(user)

  if(user.id !== null){
    if(user.isAdmin){
      return <Navigate to="/dashboard" />
    }
    else return <Navigate to="/"/>
  }
  else{
    return (
      <div className={style.container}>

      <form className={style.loginForm} onSubmit={(e) => loginUser(e)}>

        <p className={style.loginLabel}>Login</p>

        <label htmlFor="email">Email</label>
        <input className={style.email} type="email" id="email" placeholder="Enter your Email"
        value={email} onChange={(e) => setEmail(e.target.value)}/>

        <label htmlFor="password">Password</label>
        <input className={style.password} type="password" id="password" placeholder="Enter your Password" 
        value={password} onChange={(e) => setPassword(e.target.value)} />

        <Link to="/passwordReset" style={{textDecoration: "none", color: "black"}}>
          <p className={style.forgot}>Forgot Password? </p>
        </Link>   
        {

          loginIsActive 
          ?
          <button className={style.btn}>Login</button>
          :
          <button className={style.btn} disabled>Login</button>
        
        }
          <p>Don't have an account? Register  
            <Link to="/register" style={{marginLeft: "0.2rem", color: "blue"}}>
               here.
            </Link>
          </p>
        
      </form>

      </div>
    )
  }
}

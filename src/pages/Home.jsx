import { Link } from 'react-router-dom';
import Popular from '../components/Popular';
// import MostPopular from '../components/MostPopular';

import style from "../styles/home.module.css";

export default function Home() {

  return (
    <>  
      <div className={style.bg}>
          <div className={style.container}>
            <div className={style.content}>
              <h1 className={style.banner}>Find the Best & Affordable Deals for your Computer</h1>
              <Link to="/products">
                <button className={style.btn}>Buy Here</button>
              </Link>
            </div>
          </div>
      </div>
        
      <div className={style.cardContentContainer}>
        
        <CardContent title={"QUALITY PRODUCTS"}/>
        <CardContent title={"SECURED ONLINE PAYMENT"}/>
        <CardContent title={"FAST DELIVERY"}/>
        
      </div>
      <div style={{padding: "2rem 10rem", backgroundColor: "hsl(0deg, 0%, 18%)", color: "white"}}>

      <Popular />
      </div>

    </>
  )

  function CardContent({ title }) {
    return (
      <div className={style.cardContent}>
        <p>{title}</p>
      </div>
    )
  }
}

import { Link, Navigate, useNavigate } from "react-router-dom";
import { useContext } from "react";

import style from "../styles/card.module.css"
import img1 from "../assets/amd_pc.png"
import UserContext from "../UserContext";
import Swal from "sweetalert2";

export default function Card({productData}) {

  const navigate = useNavigate();
  const {user, addToCart} = useContext(UserContext);
  const { _id, name, price} = productData;

  const order = {
    productId: _id,
    productName: name,
    price: price,
    quantity: 1
  }

  return (
    <div className={style.card}>
        <div className={style.containerImg}>
            <img src={img1} alt="item"/>
        </div>
        <div className={style.body}>
        <Link style={{textDecoration: "none"}} to={`/products/${_id}`}>
          <p className={style.title}>{name}</p>
        </Link>
          <p className={style.price}>₱{price.toLocaleString()}</p>
          {
            (user.isAdmin !== true && user.isAdmin !== null) &&
            <button className={style.btn} onClick={() => addToCart(order)}>Add to Cart</button>
          }
          
        </div>
    </div>
  )
}

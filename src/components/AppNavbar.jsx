import { Link } from 'react-router-dom';

import { useContext } from 'react';
import UserContext from '../UserContext';

import nav from  "../styles/appnavbar.module.css";



export default function AppNavbar() {

    const {user, unSetUser, cart} = useContext(UserContext);

    return(
        <>
        <nav>

            <h1><Link className={nav.linkLogo} to="/">PCByte</Link></h1>
            <ul className={nav.navLinks}>
                <li><Link className={nav.link} to="/">Home</Link></li>
                <li><Link className={nav.link} to="/products">Products</Link></li>
                <li><Link className={nav.link} to="/about">About</Link></li>
                <li><Link className={nav.link} to="/contact">Contacts</Link></li>
            </ul>

            {
                (user.id !== null)
                ?
                <>
                    {/* if there is a verified user */}
                    <div className={nav.userNav}>

                        {
                            // if user is an admin render the Dashboard, if not render the cart and profile button
                            user.isAdmin 
                            ?
                            <Link className={nav.dashboard} to="/dashboard">
                                <p style={{marginBottom: "0px"}}>Dashboard</p>
                            </Link>
                            :
                            <div className={nav.cartProfile}>
                                <Link style={{color: "white"}} to="/cart">
                                    <div className={nav.cart}>
                                        {
                                            (cart.length !== 0) && 
                                            <div className={nav.count}><span>{cart?.length}</span></div>
                                        }
                                        <i className="fa-solid fa-cart-shopping"></i>
                                    </div>
                                </Link> 
                                
                                {/* <Link style={{color: "white"}} to="/profile">
                                    <div className={nav.profile}>
                                        <i className="fa-solid fa-user"></i>
                                    </div>
                                </Link> */}
                            </div>
                        }
                        <Link to="/logout">
                            <button className={nav.logoutBtn} onClick={unSetUser}>Logout</button>
                        </Link>
                    </div>
                </>
                :
                // if there is no verified user
                <div className={nav.signin}>
                    {/* login & register */}
                    <Link to="/login">
                        <button className={nav.signupBtn}>Login</button>
                    </Link>

                    <Link to="register">
                        <button className={nav.signinBtn}>Register</button>
                    </Link>
                </div>
            }
        </nav>
    </>
    )


    
}

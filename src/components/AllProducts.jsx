import {Button, Table} from 'react-bootstrap/esm';
import { useEffect, useState } from 'react';
import Swal from 'sweetalert2';
import { useNavigate } from 'react-router-dom';

export default function AllProducts() {

    const navigate = useNavigate();

    const [products, setProducts] = useState([]);


    const getAllproducts = () => {

        fetch(`${process.env.REACT_APP_API_URL}/products/allProducts`, {
            headers: {
                "Authorization": `Bearer ${localStorage.getItem("token")}`
            }
        })
        .then(res => res.json())
        .then(data => {
            setProducts(data);
        })
    }

    // Making the product inactive
    const archive = (productId, productName) => {
        fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/archive`, {
            method: "PATCH",
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${localStorage.getItem("token")}`
            },
            body: JSON.stringify({
                isActive: false
            })
        })
        .then(res => res.json())
        .then(data => {
            if(data){
                Swal.fire({
                    title: "Archive Successful!",
                    icon: "success",
                    text: `${productName} is now inactive`
                })
                getAllproducts();
            }
            else{
                Swal.fire({
                    title: "Archive unSuccessful!",
                    icon: "error",
                    text: "Something went wrong. Please try again"
                })
            }
        })
    }

    // Making the product active
    const unArchive = (productId, productName) => {
        fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/archive`, {
            method: "PATCH",
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${localStorage.getItem("token")}`
            },
            body: JSON.stringify({
                isActive: true
            })
        })
        .then(res => res.json())
        .then(data => {
            // console.log(data)
            if(data){
                Swal.fire({
                    title: "Unarchive Successful!",
                    icon: "success",
                    text: `${productName} is now active`
                })
                getAllproducts();
            }
            else{
                Swal.fire({
                    title: "Unarchive unSuccessful!",
                    icon: "error",
                    text: "Something went wrong. Please try again"
                })
            }
        })
    }

    // Product List
    const Products = products.map(product => {

        const {_id, name, description, price, stocks, isActive} = product;

        return (
                <tr key={_id}>
                    <td>{_id}</td>
                    <td>{name}</td>
                    <td>{description}</td>
                    <td>₱{price.toLocaleString()}</td>
                    <td>{stocks}</td>
                    <td style={{color: isActive && stocks > 0 ? "green" : "red"}}>{isActive && stocks > 0 ? "Available": "Not Available"}</td>
                    <td style={{display: "flex", flexDirection: "column"}}>
                    <Button variant='success' className='my-1' onClick={() => navigate(`/products/${product._id}/update`)}>Edit</Button>
                    {
                        isActive ? 
                        <Button variant='danger' className='my-1' onClick={() => archive(_id, name)}>Archive</Button>
                        :
                        <Button variant='warning' className='my-1'onClick={() => unArchive(_id, name)}>Unarchive</Button>
                    }
                    </td>
                </tr>
        )
    })

    useEffect(() => {
        getAllproducts();
    }, [products])

    return (
    <>  
        <Table striped bordered hover variant='dark' className='my-4'>
            <thead>
                <tr>
                    <th>Product ID</th>
                    <th>Product Name</th>
                    <th>Description</th>
                    <th>Price</th>
                    <th>Stocks</th>
                    <th>Status</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                {Products}
            </tbody>
        </Table>
        
    </>
  )
}
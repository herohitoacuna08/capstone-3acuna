import { useState, useEffect } from 'react';

import Card from './Card'


export default function Popular() {

    const [ popular, setPopular ] = useState([]);

    const LatestProductsCard = popular.map(product => {
        return <Card key={product._id} productData={product} />
    })

    const fetchLatestProducts = () => {

        fetch(`${process.env.REACT_APP_API_URL}/products/popular`)
        .then(res => res.json())
        .then(data => setPopular(data));
    }

    useEffect(() => {

        fetchLatestProducts();

    }, [])

    return (
        <>
        <h3>Most Popular</h3>
        <div style={{display: "flex", flexWrap: "wrap", justifyContent: "center", color: "white"}}>
            {LatestProductsCard}
        </div>
        </>
    )
}

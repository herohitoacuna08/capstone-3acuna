import React, { useEffect, useState } from 'react'
import { Button } from 'react-bootstrap';
import Swal from 'sweetalert2';

import Img from "../assets/amd_pc.png"

export default function CreateProduct() {

    const [productName, setProductname] = useState("");
    const [description, setDescription] = useState("");
    const [price, setPrice] = useState("");
    const [stocks, setStocks] = useState("");
    
    const [active, setActive] = useState(false);

    const createProduct = () => {

        fetch(`${process.env.REACT_APP_API_URL}/products/`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${localStorage.getItem("token")}`
            },
            body: JSON.stringify({
                name: productName,
                description: description,
                price: price,
                stocks: stocks
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)
            if(data){
                Swal.fire({
                    title: "Product Created!",
                    icon: "success",
                    text: `${productName} is now inactive`
                })
            }
            else{
                Swal.fire({
                    title: "Failed to Create!",
                    icon: "error",
                    text: "Something went wrong. Please try again"
                })
            }
        })

    }

      
    useEffect(() => {
        if(productName !== "" && description !== "" && price !== "" && stocks !== ""){
          setActive(true);
        }
        else setActive(false);
      }, [productName, description, price, stocks])

    return (
        <div style={{display: "flex", justifyContent: "center", padding: "2rem"}}>
            <img src={Img} alt="" style={{width: "25rem", height: "20rem", borderRadius: "10px"}}/>

            <div style={{display: "flex", justifyContent: "center", flexDirection: "column",marginLeft: "2rem"}}>

                <input type="text" placeholder="Product Name" value={productName} onChange={(e) => setProductname(e.target.value)}/>
                <textarea name="description" id="description" value={description} onChange={(e) => setDescription(e.target.value)}  cols="70" rows="8" placeholder="Description"></textarea>
                <input type="number" value={stocks} onChange={(e) => setStocks(e.target.value)}  placeholder="Stocks"/>
                <input type="number" value={price} onChange={(e) => setPrice(e.target.value)}  placeholder="Price"/>
                {
                active ? 
                <Button style={{width: "100%"}} onClick={() => createProduct()}>+ Add Product</Button>
                :
                <Button style={{width: "100%"}} disabled>+ Add Product</Button>
                }
            </div>
        </div>
    )
}

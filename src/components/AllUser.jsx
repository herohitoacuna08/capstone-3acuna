import {Button, Table} from 'react-bootstrap/esm';
import { useContext, useEffect, useState } from 'react';
import Swal from 'sweetalert2';
import UserProvider  from '../UserContext';


export default function AllUsers() {    

    const {user} = useContext(UserProvider);

    const [users, setUsers] = useState([]);

    const AllUsers = users.filter(filterUser =>filterUser._id !== user.id).map(user => {
        return (
            <tr key={user._id}>
                <td>{user._id}</td>
                <td>{`${user.firstName} ${user.lastName}`}</td>
                <td>{user.username}</td>
                <td>{user.mobileNo}</td>
                <td>{user.email}</td>
                <td style={{color: user.isAdmin ? "green" : "red"}}>{user.isAdmin ? "ADMIN": "NOT ADMIN"}</td>
                <td style={{display: "flex", flexDirection: "column"}}>
                    {
                        user.isAdmin ? 
                        <Button variant='danger' className='my-1' onClick={() => unAdmin(user._id, user.firstName)}>Not Admin</Button>
                        :
                        <Button variant='success' className='my-1' onClick={() => makeAdmin(user._id, user.firstName)}>Admin</Button>
                    }
                </td>
            </tr>   
        )
    })

    const getAllUsers = () => {

        fetch(`${process.env.REACT_APP_API_URL}/users/all`, {
            headers: {
                "Authorization": `Bearer ${localStorage.getItem("token")}`
            }
        })
        .then(res => res.json())
        .then(data => {
            // console.log(data);
            setUsers(data);
        })
    }

    const makeAdmin = async (userId, userName) => {

        // console.log(userId, userName)

        await fetch(`${process.env.REACT_APP_API_URL}/users/${userId}`, {
            method: "PATCH",
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${localStorage.getItem("token")}`
            },
            body: JSON.stringify({
                isAdmin: true
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)
            if(data){
                Swal.fire({
                    title: "Make an Admin Successful!",
                    icon: "success",
                    text: `${userName} is now an Admin`
                })
                getAllUsers();
            }
            else{
                Swal.fire({
                    title: "Make an Admin unSuccessful!",
                    icon: "error",
                    text: "Something went wrong. Please try again"
                })
            }
        })
    }

    const unAdmin = async (userId, userName) => {

        // console.log(userId, userName)

        await fetch(`${process.env.REACT_APP_API_URL}/users/${userId}`, {
            method: "PATCH",
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${localStorage.getItem("token")}`
            },
            body: JSON.stringify({
                isAdmin: false
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)
            if(data){
                Swal.fire({
                    title: "Remove Admin Successful!",
                    icon: "success",
                    text: `${userName} is remove as an admin`
                })
                getAllUsers();
            }
            else{
                Swal.fire({
                    title: "Remove Admin unSuccessful!",
                    icon: "error",
                    text: "Something went wrong. Please try again"
                })
            }
        })
    }

    useEffect(() => {
        getAllUsers();
    }, [users])

  return (
    <>
        <Table striped bordered hover variant='dark' className='my-4'>
            <thead>
                <tr>
                    <th>User ID</th>
                    <th>Full Name</th>
                    <th>Username</th>
                    <th>Mobile No.</th>
                    <th>Email</th>
                    <th>isAdmin</th>
                    <th style={{width: "150px"}}></th>
                </tr>
            </thead>
            <tbody> 
                {AllUsers}  
            </tbody>
        </Table>
    </>
  )
}
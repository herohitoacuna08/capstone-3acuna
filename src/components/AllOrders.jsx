import { Table} from 'react-bootstrap/esm';
import { useEffect, useState } from 'react';

export default function AllOrders() {

    const [orders, setOrders] = useState([]);

    const getAllOrders = () => {

        fetch(`${process.env.REACT_APP_API_URL}/orders/all`, {
            headers: {
                "Authorization": `Bearer ${localStorage.getItem("token")}`
            }
        })
        .then(res => res.json())
        .then(data => {
            // console.log(data);
            setOrders(data);
        })
    }

    useEffect(() => {
        getAllOrders();
    }, [])


    const Orders = orders.map(order => {
        return (
                <tr key={order._id}>
                    <td>{order._id}</td>
                    <td>{order.orderedOn}</td>
                    <td>{order.productName}</td>
                    <td>{order.username}</td>
                    <td>{order.quantity}</td>
                    <td>{order.price.toLocaleString()}</td>
                    <td>₱{order.totalAmount.toLocaleString()}</td>
                </tr>
        )
    })

  return (
    <>
        <Table striped bordered hover variant='dark' className='my-4'>
            <thead>
                <tr>
                    <th>Order ID</th>
                    <th>Date</th>
                    <th>Product Name</th>
                    <th>Username</th>
                    <th>Quantity</th>
                    <th>Price</th>
                    <th>Total</th>
                </tr>
            </thead>
            <tbody>
            {Orders}
            </tbody>
        </Table>
    </>
  )
}